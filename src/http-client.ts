import axios from 'axios'

export const httpClient = {
    async get(url: string, options: any) {
        return axios.get(url)
    }
}
