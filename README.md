# Gaia-X jsonld http-client

Simple HTTP client replacement for `@digitalbazaar/http-client` using `axios` and without relying on `ky` nor wasm

## Usage

Install the package

```shell
npm i --save @gaia-x/jsonld-http-client
```

Edit `package.json`
```
{
  "overrides": {
    "@digitalbazaar/http-client": "npm:@gaia-x/jsonld-http-client"
  }
}
```

Your package-lock.json should show. If not the case, delete the entry, remove the node modules and `npm install` again
```json
{
  "node_modules/@digitalbazaar/http-client": {
    "name": "@gaia-x/http-client",
    "version": "xxx",
    ...
  }
```