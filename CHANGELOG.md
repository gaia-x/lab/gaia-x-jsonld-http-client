# 1.0.0-development.1 (2024-02-28)


### Features

* first version ([4084fa7](https://gitlab.com/gaia-x/lab/gaia-x-jsonld-http-client/commit/4084fa78cdb962052aad7e0d9cab465bb614d5e5))
